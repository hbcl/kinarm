**KINARM HOW TO USE

Placeholder for matlab code for kinarm data analysis. 

All analysis functions are placed in one, KinarmProcess. Use 'demo' to run code on a default dataset which shows basic functionality including hand position/speed and inverse dynamics. 