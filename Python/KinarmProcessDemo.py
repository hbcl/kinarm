#%%
#import 
import numpy as np
import scipy.io as scp
import kinarmProcess_py39 as kp
import matplotlib.pyplot as plt
import os

# assume that you are in the ~/git/kinarm directory:
kinarmPath = os.path.dirname(kp.__file__)

testdata = scp.loadmat(kinarmPath + '/Testdata/TestDataPrefReach.mat') #load test data (change path)

LPRHXData = kp.lowpassSimple(testdata["Right_HandX"],1000,12)

tanvel = kp.handSpeedPlot(testdata["Right_HandX"], testdata["Right_HandY"], sr=1000)

force = testdata["torque"]/[0.5*np.sin(testdata["angle"])]

fraterate = kp.forceRateRate(force,1000,12)

time = testdata["time"]
plt.plot(time,tanvel,'b')
plt.xlabel('time (s)')
plt.ylabel('tangential velocity (m/s)')
plt.show()
#%%