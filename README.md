# KINARM Use
The KinARM is Professor Tyler Cluff's. Please be very very respectful of the time (booking off the kinarm is at the exclusion of others), noise (it's a shared space), 
and equipment in Tyler Cluff's and Ryan Peters' space. 

# KINARM data analysis
The Kinarm analysis code is currently leveraging the object oriented nature of Matlab to keep the number of files to ~1. <br>

<p>
This means that all functions within the file 'kt.m', as technically **static** functions, need to be called with the prefix **kt.**. <br>
I.e., kt.loadzipSortSave(fname), kt.stackedKinematicsAndTorques(...). <br>
<p>
## Quickstart (MATLAB)
Add the entire directory of the repository/MATLAB with addpath(), along with the subdirectory MATLAB/matlab_kinarm.
run kt.demo($fname), where $fname is some file (either .mat or .zip). <br>

<h1>KINARM SIMULINK INSTRUCTIONS</h1>

In "Creating task programs for dexterit-e.pdf" <br>

<h2> Gotchas</h2>

1. Pathing is an issue for each version of Dexterit-e; not every version is compatible. Read 2.6 and set the path appropriately by shuffling up or down depending on which dexterite you're using.  <br>
2. Be careful assuming things about the kinarm codebase--there are in some places the same variable redefined a few times. <br>
3. within the Simulink file at the top there is 'Parameter Table Defn' where you can add parameters. <br>

---

<h2> Create a file </h2>

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.


<h2> Generic bitbucket info: Clone a repository </h2>

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).