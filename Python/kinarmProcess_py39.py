# kinarmProces_py39
#%%
#import 
#from dataclasses import dataclass
#from fnmatch import fnmatchcase
import numpy as np
from scipy import signal

# lowpass lowpassSimple
def lowpassSimple(data,samprate,cutoff,order=2,direction=2):    
    # get filter paramters A and B
    #halved = samprate*0.5
    Wn = cutoff/(samprate/2)
    B,A = signal.butter(order,Wn)
      # perform filtering
    if direction==2:
       newdata = signal.filtfilt(B,A,data,padlen=0)
    else:
       newdata = filter(B,A,data)
    return newdata  
    
# Find tanvel
def handSpeedPlot(posx, posy, sr=1000):
    x = lowpassSimple(posx,sr,12)
    vx = np.gradient(x,1/sr,axis=0)
    y = lowpassSimple(posy,sr,12)
    vy = np.gradient(y,1/sr,axis=0)
    tanvel = np.sqrt(((vx**2)+(vy**2)))
    vx = vx
    vy = vy
    return tanvel

# forceRateRate
def forceRateRate(force,sr,cutoff):
      forceLP = lowpassSimple(force,sr,cutoff)
      frate = np.gradient(forceLP,1/sr,axis=1)
      fraterate = np.gradient(frate,1/sr,axis=1)
      return fraterate
      
# loadZipSortSave
#def loadZipSortSave(fname,varargin):
      #     function loadZipSortSave(fname,varargin)
      # loads the .zip file, sorts it, and saves the data in a .mat file for
      # future use.
      # by default: 'sort': sorts by execution (could also be 'tp' or 'custom'; see matlab_KINARM/sort_trials())
      #             'savedir' saves to current directory
      #             'savename' appends 'Sorted' to the filename for saving,
      
      ### processing optional args
      # defaults
      #fnameSave = [fname,'Sorted']
      #dirSave = pwd()
      #sortMethod = 'execution'
      #x = len(varargin)
      #for i in range ([1 : 2 : x ]):
        #option = varargin[i]
        #value = varargin[i + 1]
        #if option[i] == 'savename':
            #fnameSave = value
         #elif option[i] == 'savedir':
            #dirSave = value
         #elif option[i] == 'sort':
           # sortMethod= value
      ### done processing 
      #data = zip_load(fname)
      #data = sort_trials(data,sortMethod)
      #np.save('fnameSave.npy'),[data,fname])
    
# %%
