#%%
import sys
sys.path.append("/Users/jeremy/Git/optreach")

import ReachingModels as reach
import casadi as ca
import SimpleOpt as so
import numpy as np
import os 
import scipy as sci
import matplotlib.pyplot as plt

optreachPath = os.path.dirname('/Users/jeremy/Git/optreach/')
kinarmAnalysisPath = os.path.dirname('/Users/jeremy/Git/kinarmexperiments/PreferredReach/')
modelfitPath = os.path.dirname('/Users/jeremy/Git/kinarmexperiments/PreferredReach/Model Fit/')

# 
def peakspeedDurationDistanceForExperimentalConditions(mod:so.SimpleModel,opt:so.optiParam,ct,s_xy0,s_xy1):
  n = s_xy0.shape[0]
  m_peakspeed = np.zeros(n)
  m_duration = np.zeros(n)
  m_distance = np.zeros(n)
  
  for (i,x0,y0,x1,y1) in zip(range(0,s_xy0.shape[0]),s_xy0[:,0],s_xy0[:,1],s_xy1[:,0],s_xy1[:,1]):
    traj,optout = mod.updateGuessAndSolve(opt,xystart=np.array([x0,y0]),xyend=np.array([x1,y1]),theTimeValuation= ct, theFRCoef = 8.5e-2,theGeneratePlots=0)
    m_peakspeed[i] = max(traj.handspeed)
    m_duration[i]  = traj.time[-1]
    m_distance[i]  = traj.distance

  return m_peakspeed, m_duration, m_distance

# error function to fit. 
def errorFromTimeValuationFitToData(mod:so.SimpleModel, opt:so.optiParam, ct,s_xy0,s_xy1, s_peakspeed, s_duration):
  m_peakspeed, m_duration, m_distance = peakspeedDurationDistanceForExperimentalConditions(mod,opt,ct,s_xy0,s_xy1)
  
  # select normalization constants for the two errors. 
  norm_speed = 0.75
  norm_duration = 1.5
  
  # normalize the errors
  errspd = ca.sumsqr(m_peakspeed - s_peakspeed)**2/norm_speed**2
  errdur = ca.sumsqr(m_duration - s_duration)**2 /norm_duration**2
  return errspd + errdur

# read in the data given a file name; return start and end xy, speed, duration, and (performed) distance.
def unpackFileSpeedDurationData(thefname):
  """
  def unpackFileSpeedDurationData(thefname):
  input: file name
  output: start and end xy, speed, duration, and (performed) distance.
  """
  tgtdict = sci.io.loadmat(optreachPath + '/empirical/subdata/' + thefname) #load test data (change path)
  curspd  = tgtdict["peakspeed"][:,0]
  curdur  = tgtdict["duration"][:,0]
  curxy0  = tgtdict["xymeanstart"]
  curxy1  = tgtdict["xymeanend"]
  distance_perf  = tgtdict["perfdistance"][:,0]
  return curxy0, curxy1, curspd, curdur, distance_perf


def visualizeSpeedDurationData(curxy0,curxy1,curspd,curdur,curdist):
  """
  def visualizeSpeedDurationData(curxy0,curxy1,curspd,curdur,curdist):
  input: start and end xy, speed, duration, and (performed) distance.
  quickanddirty: plot xy s/e, speed dist using matplotlib on a 2x2 grid.
  output: matplotlib axis object.
  """

  # quickanddirty: plot xy s/e, speed dist
  fig,ax = plt.subplots(2,2)
  ax[0,0].plot(curdist,curspd,color = 'green',marker='o',linestyle='none')
  ax[0,0].set_xlim([0,.5])
  ax[0,0].set_ylim([0,1])

  ax[0,1].plot(curdist,curdur,color = 'green',marker='o',linestyle='none')
  ax[0,1].set_xlim([0,.5])
  ax[0,1].set_ylim([0,2.5])

  ax[1,1].plot(curxy0[:,0],curxy0[:,1],color = 'green',marker='o',linestyle='none')
  ax[1,1].plot(curxy1[:,0],curxy1[:,1],color = 'red',marker='s',linestyle='none')
  for ind,el in enumerate(curxy1[:,0]):
    ax[1,1].plot(np.array([curxy0[ind,0],curxy1[ind,0]]), np.array([curxy0[ind,1],curxy1[ind,1]]))
  ax[1,1].set_xlim([-.4,0])
  ax[1,1].set_ylim([.3,.8])
  return ax[0,0]
  

def optTimeValFit(inmod, inopt, curxy0, curxy1, curspd, curdur):
  """
  def optTimeValFit(inmod, inopt, curxy0, curxy1, curspd, curdur):
  input: model, optiparam, start and end xy, speed, duration.
  output: optimal time valuation, speed, duration, distance, and optout object.
  """
  # make a lambda function to use with nelder-mead.
  costSSE = lambda cur_ct:errorFromTimeValuationFitToData(inmod, inopt, cur_ct, curxy0, curxy1, curspd, curdur)
  
  # call neldermead to get a fit. 
  optout  = sci.optimize.minimize(costSSE,10,bounds=sci.optimize.Bounds(lb=.01,ub=300),method='Nelder-Mead',tol=1e-2)
  
  # parse the return value. 
  tvalopt = optout.x[0]

  # return the optimal tval speed, and dur. 
  spd,dur, dist = peakspeedDurationDistanceForExperimentalConditions(inmod, inopt, tvalopt, curxy0, curxy1)
  return tvalopt, spd, dur, dist, optout

def optTimeValuationGradientDescent(inmod, inopt, curxy0, curxy1, curspd, curdur, dstep = 1e-2):
  """
  def optTimeValuationGradientDescent(inmod, inopt, curxy0, curxy1, curspd, curdur):
  input: model, optiparam, start and end xy, speed, duration.
  output: optimal time valuation, speed, duration, distance, and optout object.
  Method: 
  - goal: find T_C that minimizes the error between model and human.
  - begin with initial guess. 
  - compute error between model and human
  - take a step in cost of time, compute SSError
  - dSSE/dT_C is approximately linear in T_C, so we can use a line search to find the minimum.
  - take a step in the direction of the minimum, scaled by the step size variable, dstep. 
  - repeat until convergence.
  - CONCERN: stepsize might be too large, and noisy gradient might slow down convergence.
  """
  # make a lambda function to use with nelder-mead.
  costSSE = lambda cur_ct:errorFromTimeValuationFitToData(inmod, inopt, cur_ct, curxy0, curxy1, curspd, curdur,
    theNodes = 20)

  tValGuess = initialTimeValuation(curxy0, curxy1, curspd, curdur)
  
  err = 1e1
  while err > 1e-2:
    # compute the error at the current time valuation. 
    err = costSSE(tValGuess)

    # compute the gradient of the error with respect to time valuation. 
    grad = (costSSE(tValGuess + dstep) - costSSE(tValGuess - dstep))/(2*dstep)

    # compute the step size. 
    step = -dstep*grad

    # take a step in the direction of the gradient. 
    tValGuess = tValGuess + step

def initialTimeValuation(curxy0, curxy1, curspd, curdur):
  """
  def initialTimeValuation(curxy0, curxy1, curspd, curdur)
  input: start and end xy, speed, duration.
  output: initial time valuation.
  """
  # compute the distance between the start and end xy. 
  dist = np.sqrt(np.sum((curxy0 - curxy1)**2,axis=1))

  # compute the average speed. 
  avespd = np.mean(curspd)

  # compute the average duration. 
  avedur = np.mean(curdur)

  # compute the average distance. 
  avedist = np.mean(dist)

  # compute the initial time valuation. 
  tValGuess = avedist/(avespd*avedur)

  return tValGuess

def loopfitAllSubs(nums = range(1,10), N = 20):
  """
  def loopfitAllSubs(nums = range(1,10), N = 20):
  input: list of subject numbers, number of points to use in the model.
  output: list of time valuations, speeds, durations, distances, and optout objects.
  """
  tvals = list()
  mspds  = list()
  mdurs  = list()
  ispds  = list()
  idurs  = list()
  idists = list()
  mdists = list()
  nmopts   = list()

  for i in nums:
    fname   = 'tgtsNoAccPrefSub' + str(i) + '.mat'
    curxy0, curxy1, curspd, curdur, curdist = unpackFileSpeedDurationData(fname)
    modname = 'modelSub' + str(i) + '.mat'
    model   = reach.Kinarm(modname)
    oP   = model.movementTimeOptSetup(theN = N)

    tv,sp,du, dist, neldmeadopt = optTimeValFit(model, oP, curxy0, curxy1, curspd, curdur)
    
    tvals.append(tv)
    mspds.append(sp)
    mdurs.append(du)
    mdists.append(dist)
    idists.append(curdist)
    ispds.append(curspd)
    idurs.append(curdur)
    nmopts.append(neldmeadopt)
  
  return tvals, idists, ispds, idurs, mdists, mspds, mdurs, nmopts 
  
def inspectfit(hps,hdu,hdi,mps,mdu,mdi):
  """
  def inspectfit(hps,hdu,hdi,mps,mdu,mdi):
  input: speeds durations distances for model and subject. 
  output: matplotlib axis object.
  """
  f,ax = plt.subplots(2,2)
  f.tight_layout(h_pad = 2,w_pad = 2)
  ax[0,0].plot(hdi,hdu,linestyle = 'none',marker='.')
  ax[0,0].plot(mdi,mdu,linestyle = 'none',marker='.')

  ax[0,1].plot(hdi,hps,linestyle = 'none',marker='.')
  ax[0,1].plot(mdi,mps,linestyle = 'none',marker='.')
  return ax

def loopAndSaveSimFit(fname):
  tvals, idists, ispds, idurs, mdists, mspds, mdurs, nmopts = loopfitAllSubs(nums = range(1,10), N = 20)
  saveDict = {
    "timeValuation":tvals,
    "peakspeedm":mspds,
    "distsm":mdists,
    "durationm":mdurs,
    "peakspeed":ispds,
    "dists":idists,
    "duration":idurs}
  sci.io.savemat(modelfitPath+'/'+fname+'.mat', saveDict)
# %%

